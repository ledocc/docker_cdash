FROM root/minbase
MAINTAINER David Callu ledocc@gmail.com



ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y unzip git apache2 mysql-server php5 php5-mysql php5-xsl php5-curl php5-gd


RUN mkdir root/install
ADD http://www.cdash.org/wp-content/uploads/2014/11/CDash-2-2-3.zip /tmp/
ADD cdash.conf config.local.php install.sh /root/install/
RUN root/install/install.sh

EXPOSE 80

ADD start.sh /root/
ENTRYPOINT ["/root/start.sh"]
