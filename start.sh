#!/bin/bash
# Note: I've written this using sh so it works in the busybox container too

# USE the trap if you need to also do manual cleanup after the service is stopped,
#     or need to start multiple services in the one container
trap "echo TRAPed signal" HUP INT QUIT KILL TERM

# start service in background here
#/usr/sbin/apachectl start
service mysql start
service apache2 start

echo "[hit enter key to exit] or run 'docker stop <container>'"
read

# stop service and clean up here
#echo "stopping apache"
#/usr/sbin/apachectl stop

service apache2 stop
service mysql stop

echo "exited $0"
