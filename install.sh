#!/bin/sh -xe


init_mysql()
{
    /usr/bin/mysqld_safe &
    sleep 5

    mysql -u root -e "CREATE USER 'cdash_user'@'localhost' IDENTIFIED BY 'cdash_passwd'"

    mysql -u root -e "CREATE DATABASE cdash_db;"

    mysql -u root -e "GRANT ALL ON cdash_db.* TO 'cdash_user'@'localhost';"
    mysql -u root -e "FLUSH PRIVILEGES;"

}

init_mysql


FILE_DIR=/root/install

CDash_ROOT=/var/www/CDash

CDash_VERSION=2-2-3
CDash_UNZIPPED_DIR=CDash-${CDash_VERSION}
CDash_DOWNLOAD_FILE=${CDash_UNZIPPED_DIR}.zip

DOWNLOAD_DIR=/tmp
INSTALL_DIR=/var/www

APACHE_USER=www-data

unzip -d ${INSTALL_DIR} ${DOWNLOAD_DIR}/${CDash_DOWNLOAD_FILE}
rm -vrf ${DOWNLOAD_DIR}/${CDash_DOWNLOAD_FILE}
mv -v ${INSTALL_DIR}/${CDash_UNZIPPED_DIR} ${INSTALL_DIR}/CDash
cp ${FILE_DIR}/config.local.php ${INSTALL_DIR}/CDash/cdash/

chown -vR ${APACHE_USER}:${APACHE_USER} ${INSTALL_DIR}/CDash

cp -v ${FILE_DIR}/cdash.conf /etc/apache2/sites-available/
a2dissite 000-default.conf
a2ensite cdash.conf
